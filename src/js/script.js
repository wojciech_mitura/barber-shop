window.addEventListener('load',function() {
    
    function burgerMenuHandler() {
        const burger = document.getElementById('burger')
        const navList = document.getElementById('navList');
        
        burger.addEventListener('click', function() {    
            burger.classList.toggle("active");
            navList.classList.toggle("active");
        });  

        navList.addEventListener("click", function() {
            burger.classList.remove("active");
            navList.classList.remove("active");
        }) 
    }
    function slickSliderHandler() {
        const prevButton = document.getElementsByClassName("btn-prev")[0];
        const nextButton = document.getElementsByClassName("btn-next")[0];

        $('.slick-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            cssEase: 'linear',
            prevArrow: prevButton,
            nextArrow: nextButton,
            dots: false,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 2000
        });
    }

    burgerMenuHandler();
    slickSliderHandler();
    $('.parallax-effect').parallax();
});